require 'rubygems'
require 'json'
require 'bundler/setup'
require 'sinatra'
require 'sinatra/custom_logger'
require 'logger'
require 'require_all'
require 'net/http'

Bundler.require(:default, 'development')

Dotenv.load
require_all 'app/models'
require_all 'app/controllers'

$redis = Redis.new(:host => ENV["REDIS_HOST"], :port => ENV["REDIS_PORT"])