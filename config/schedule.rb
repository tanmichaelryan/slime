# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every '0 17 * * MON-FRI' do
  set :job_template, "zsh -l -c ':job'"
  set :output, "log/cron.log"
  rake "attendance:check_absence"
end

every '0 20 * * MON-FRI' do
  set :job_template, "zsh -l -c ':job'"
  set :output, "log/cron.log"
  rake "attendance:check_absence_rude"
end

every '0 0 * * MON-FRI' do
  set :job_template, "zsh -l -c ':job'"
  set :output, "log/cron.log"
  rake "attendance:reset_attendance"
end

every '0 11 * * MON-FRI' do
  set :job_template, "zsh -l -c ':job'"
  set :output, "log/cron.log"
  rake "link:important_link"
end