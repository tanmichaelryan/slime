class BaseController < Sinatra::Base

  use Rack::PostBodyContentTypeParser

  post "/slime" do
    case event["type"]
    when Event::TYPE[:message]
      if event["subtype"] == Event::SUBTYPE[:bot_message]
        attending if bot_id == "B01043HH42G"
      end
    when Event::TYPE[:app_mention]
      app_mention_event
    when Event::TYPE[:user_change]
      status_update
    end
    return 'OK'
  end

  private

  def attending
    name = get_name
    puts "deleting #{name}"
    Attendance.attend(name)
  end

  def check_attendance
    response = "User yang belum absen: "
    Attendance.absence_members.each do |member|
      response << "\n<@#{member}> | #{member}"
    end
    Event.send_message(response, true)
  end

  def status_update
    return unless Attendance.is_member?(user_id)
    case
    when Attendance.onleave_status?(status_emoji)
      Attendance.add_to_onleave(user_id)
    when Attendance.sick_status?(status_emoji)
      Attendance.add_to_sick(user_id)
    else
      Attendance.remove_onleave(user_id)
      Attendance.remove_sick(user_id)
    end
  end

  def send_runbook_url
    Event.send_runbook_url
  end

  def send_onleave_members
    onleave_members = Attendance.onleave_members

    if onleave_members.empty?
      response = "Lagi ga ada yg cuti nih"
    else
      response = "Daftar yang lagi cuti"
      onleave_members.each do |member|
        response << "\n<@#{member}>"
      end
    end
    Event.send_message(response, false)
  end

  def app_mention_event
    case get_command_on_app_mention
    when 'members'
      check_attendance
    when 'runbooks'
      send_runbook_url
    when 'onleave'
      send_onleave_members
    when 'help'
      Event.send_message('DIH', true)
    end
  end
  
  def get_name
    event_text.slice(/<@(.+)>/)&.delete("<@")&.delete('>') 
  end

  def event
    params["event"] rescue []
  end

  def event_text
    event["text"] rescue "missing params"
  end

  def event_username
    event["username"] rescue ""
  end

  def bot_id
    event["bot_id"] rescue ""
  end

  def user
    event["user"] rescue {}
  end

  def user_id
    user["id"] rescue ""
  end

  def profile
    user["profile"] rescue {}
  end

  def status_emoji
    profile["status_emoji"] rescue ""
  end
  
  def get_command_on_app_mention
    event_text.split(' ')[1]
  end

end
