module Event
  TYPE = {
    message:      "message",
    app_mention:  "app_mention",
    user_change:  "user_change"
  }

  SUBTYPE = {
    bot_message:  "bot_message" 
  }

  def self.send_message(text, test = false)
    if test 
      HTTParty.post(ENV['TEST_WEBHOOK_URL'], body: { text: text}.to_json)
    else
      HTTParty.post(ENV['WEBHOOK_URL'], body: { text: text}.to_json)
    end
  end

  def self.send_runbook_url
    text = "Runbook paid promotion bisa diliat disini ya #{ENV['RUNBOOK_URL']}"
    HTTParty.post(ENV['BE_CHANNEL_URL'], body: { text: text}.to_json)
  end

  def self.send_important_link	
    text = []
    text << divider
    text << section_block("<#{ENV['RUNBOOK_URL']}|Runbook>")
    text << section_block("<#{ENV['TECHDEBT_URL']}|Technical Debt>")
    text << section_block("<#{ENV['TOGGLE_URL']}|Feature Toggle>")
    text << section_block("<#{ENV['CHANGEDOC_URL']}|Change Document>")
    text << section_block("<#{ENV['PRODUCT_LIST_URL']}|Product List>")
    text << divider
    a = HTTParty.post(ENV['BE_CHANNEL_URL'], body: { blocks: text}.to_json)
    puts a
  end

  private

  def self.section_block(text)
    return {'type': 'section','text': {'type': 'mrkdwn','text': text}}
  end

  def self.divider
    return {'type': 'divider'}
  end

end