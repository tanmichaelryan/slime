module Attendance
  extend self

  LAST_ATTENDANCE_KEY = 'last_attendance_key'.freeze
  ATTENDANCE_KEY      = 'attendance_key'.freeze
  LAST_ATTEND_KEY     = 'last_attend_key'.freeze
  ONLEAVE_KEY         = 'onleave_key'.freeze
  SICK_KEY            = 'sick_key'.freeze
  SICK_EMOJI_KEY      = [':face_with_thermometer:'].freeze
  ONLEAVE_EMOJI_KEY   = [':palm_tree:'].freeze
  
  def is_member?(id)
    ENV['MEMBERS'].split(',').include? id
  end

  def absence_members
    $redis.smembers(ATTENDANCE_KEY) - onleave_members - sick_members
  end

  def attend(name)
    $redis.srem(ATTENDANCE_KEY, name)
    $redis.set(LAST_ATTENDANCE_KEY, name)
  end
  
  def last_attend
    $redis.get(LAST_ATTENDANCE_KEY)
  end

  def reset_attendance
    $redis.sadd(ATTENDANCE_KEY, default_members)
  end

  def onleave_status?(emoji)
    ONLEAVE_EMOJI_KEY.include? emoji
  end

  def onleave_members
    $redis.smembers(ONLEAVE_KEY)
  end

  def add_to_onleave(id)
    $redis.sadd(ONLEAVE_KEY, id)
  end

  def remove_onleave(id)
    $redis.srem(ONLEAVE_KEY, id)
  end

  def sick_status?(emoji)
    SICK_EMOJI_KEY.include? emoji
  end

  def sick_members
    $redis.smembers(SICK_KEY)
  end

  def add_to_sick(id)
    $redis.sadd(SICK_KEY, id)
  end

  def remove_sick(id)
    $redis.srem(SICK_KEY, id)
  end

  private

  def default_members
    ENV['MEMBERS'].split(',')
  end

end